﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DapperSample
{
    class Program
    {
        static string connStr;

        static void Main(string[] args)
        {
            connStr =
$@"Server = localhost; Database = Nutshell; User Id = user_test; Password = qwerty12345; Persist Security Info = False";

            SqlMapper.Settings.CommandTimeout = 600; // dapper global command timeout

            Console.WriteLine("1 ---------------------------------------------");
            SimpleSelect();

            Console.WriteLine("2 ---------------------------------------------");
            SelectWithCondition();

            Console.WriteLine("3 ---------------------------------------------");
            QueryMultiple();

            Console.WriteLine("4 ---------------------------------------------");
            GetOneValue();

            Console.WriteLine("5 ---------------------------------------------");
            Insert();

            Console.WriteLine("6 ---------------------------------------------");
            Update();

            Console.WriteLine("7 ---------------------------------------------");
            Delete();

            Console.WriteLine("8 ---------------------------------------------");
            QueryMultiMappingOneToOne();

            Console.WriteLine("9 ---------------------------------------------");
            QueryMultiMappingOneToMany();

            Console.WriteLine("10 ---------------------------------------------");
            TransactionSample();

            Console.WriteLine("11 ---------------------------------------------");
            CallProcedureWithReadingTableResult();

            Console.WriteLine("12 ---------------------------------------------");
            CallFunctionReturnOneValue();

            Console.WriteLine("13 ---------------------------------------------");
            CallFunctionReturnTable();

            Console.WriteLine("14 ---------------------------------------------");
            CallProcedureReturnTableWithNonStandardColumns();

            Console.WriteLine("15 ---------------------------------------------");
            CallFunctionReturnTableWithNonStandardColumns();

            Console.WriteLine("16 ---------------------------------------------");
            CallProcedureReturnOneValue();
        }

        // 1
        private static void SimpleSelect()
        {
            using (var context = new SqlConnection(connStr))
            {
                var res = context.Query<Customer>("SELECT id, name FROM customer").ToList();

                foreach (var item in res)
                    Console.WriteLine($@"{item.Id}, {item.Name}");
            }
        }

        // 2
        private static void SelectWithCondition()
        {
            using (var context = new SqlConnection(connStr))
            {
                var someId = 2;
                var accountDateCreate = new DateTime(2019, 07, 14);

                var res = context.Query<Customer>(
                    "SELECT id, name FROM customer WHERE id > @someId AND DateCreate < @date",
                    new { someId, date = accountDateCreate }).ToList();

                foreach (var item in res)
                    Console.WriteLine($@"{item.Id}, {item.Name}");
            }
        }

        // 3
        private static void QueryMultiple()
        {
            using (var context = new SqlConnection(connStr))
            {
                var sql = @"SELECT id, name FROM customer WHERE id = @id;
                            SELECT id, customerId, date, description, price FROM purchase WHERE customerId = @id;";

                using (var multi = context.QueryMultiple(sql, new { id = 2 }))
                {
                    var cust = multi.Read<Customer>().FirstOrDefault();
                    var purchases = multi.Read<PurchaseModel>().ToList();

                    Console.WriteLine($"Customer name: {cust.Name}");

                    foreach (var item in purchases)
                        Console.WriteLine($@"{item.Description}, {item.Price}");
                }
            }
        }

        // 4
        private static void GetOneValue()
        {
            using (var context = new SqlConnection(connStr))
            {
                var res = context.Query<string>("SELECT Name FROM customer WHERE id = @id",
                    new { id = 2 }).FirstOrDefault();

                Console.WriteLine(res);
            }
        }

        // 5
        private static void Insert()
        {
            using (var context = new SqlConnection(connStr))
            {
                var newRowId = context.Query<int>(
                    @"INSERT INTO customer(Name, DateCreate) VALUES (@name, @date);
                      SELECT CAST(SCOPE_IDENTITY() as int)",
                    new
                    {
                        name = "Jeff",
                        date = DateTime.Now
                    }).FirstOrDefault();

                Console.WriteLine($"newRowId: {newRowId}");

                SimpleSelect();
            }
        }

        // 6
        private static void Update()
        {
            using (var context = new SqlConnection(connStr))
            {
                var id = 1;
                var name = "BILL";

                var res = context.Execute(
                    "UPDATE Customer SET Name = @name WHERE id = @id",
                    new { id, name });

                SimpleSelect();
            }
        }

        // 7
        private static void Delete()
        {
            using (var context = new SqlConnection(connStr))
            {
                var affectedrows = context.Execute("DELETE FROM Customer WHERE name = @name",
                    new { name = "Jeff" });

                Console.WriteLine(affectedrows);
            }
        }

        // 8
        private static void QueryMultiMappingOneToOne()
        {
            using (var context = new SqlConnection(connStr))
            {
                var res = context.Query<CustomerAndPurchase, PurchaseModel, CustomerAndPurchase>(
                    @"SELECT cust.*, p.* FROM customer cust
	                    JOIN purchase p ON cust.ID = p.CustomerID WHERE cust.id IN @idList",
                    (customer, purchase) =>
                    {
                        customer.Purchase = purchase;
                        return customer;
                    }, new { idList = new int[] { 1, 2 } }
                    ).ToList();

                foreach (var cust in res)
                    Console.WriteLine($@"{cust.Id}, {cust.Name}, {cust.Purchase.Description}, {cust.Purchase.Price}");
            }
        }

        // 9
        private static void QueryMultiMappingOneToMany()
        {
            using (var context = new SqlConnection(connStr))
            {
                var custDictionary = new Dictionary<int, CustomerAndPurchaseList>();

                var res = context.Query<CustomerAndPurchaseList, PurchaseModel, CustomerAndPurchaseList>(
                    @"SELECT cust.*, p.* FROM customer cust
	                    JOIN purchase p ON cust.ID = p.CustomerID WHERE cust.id IN @idList",
                    (customer, purchase) =>
                    {
                        if (!custDictionary.TryGetValue(customer.Id, out CustomerAndPurchaseList custEntry))
                        {
                            custEntry = customer;
                            custEntry.Purchases = new List<PurchaseModel>();
                            custDictionary.Add(custEntry.Id, custEntry);
                        }

                        custEntry.Purchases.Add(purchase);
                        return custEntry;

                    }, new { idList = new int[] { 1, 2 } }
                    ).Distinct().ToList();

                foreach (var cust in res)
                {
                    Console.WriteLine($@"{cust.Id}, {cust.Name}:");
                    foreach (var item in cust.Purchases)
                    {
                        Console.WriteLine($"\t{item.Description}, {item.Price}");
                    }
                }
            }
        }

        // 10
        private static void TransactionSample()
        {
            try
            {
                using (var context = new SqlConnection(connStr))
                {
                    context.Open();
                    using (var transaction = context.BeginTransaction())
                    {
                        context.Execute(@"INSERT INTO customer(Name, DateCreate) VALUES (@name, @date)",
                            new { name = "Pit", date = DateTime.Now }, transaction);

                        context.Execute(@"INSERT INTO customer(Name, DateCreate) VALUES (@name, @date)",
                            new { name = "Alice", date = DateTime.Now }, transaction);

                        transaction.Commit();
                        Console.WriteLine("Done.");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        // 11
        private static void CallProcedureWithReadingTableResult()
        {
            using (var context = new SqlConnection(connStr))
            {
                var rows = context.Query<PurchaseModel>("GetPurchasesByYear", new { year = 2006 },
                        commandType: CommandType.StoredProcedure).ToList();

                foreach (var row in rows)
                {
                    Console.WriteLine($"{row.CustomerId}, {row.Date}, {row.Description}, {row.Price}");
                }
            }
        }

        // 12
        private static void CallFunctionReturnOneValue()
        {
            using (var context = new SqlConnection(connStr))
            {
                var res = context.Query<decimal>("select dbo.GetAmoutByYear (@year)",
                    new { year = 2007 }).FirstOrDefault();

                Console.WriteLine(res);
            }
        }

        // 13
        private static void CallFunctionReturnTable()
        {
            using (var context = new SqlConnection(connStr))
            {
                var rows = context.Query<PurchaseModel>("select * from fn_GetPurchasesByYear (@year)",
                    new { year = 2008 }).ToList();

                foreach (var row in rows)
                {
                    Console.WriteLine($"{row.CustomerId}, {row.Date}, {row.Description}, {row.Price}");
                }
            }
        }

        // 14
        private static void CallProcedureReturnTableWithNonStandardColumns()
        {
            using (var context = new SqlConnection(connStr))
            {
                SqlMapper.SetTypeMap(typeof(PurchaseModelNonStandard_for_proc),
                    new ColumnAttributeTypeMapper<PurchaseModelNonStandard_for_proc>());

                var rows = context.Query<PurchaseModelNonStandard_for_proc>("GetPurchasesByYearWithNonStandardColumns", new { year = 2006 },
                        commandType: CommandType.StoredProcedure).ToList();

                foreach (var row in rows)
                {
                    Console.WriteLine($"{row.CustomerId}, {row.Date}, {row.Description}, {row.Price}");
                }
            }
        }

        // 15
        private static void CallFunctionReturnTableWithNonStandardColumns()
        {
            using (var context = new SqlConnection(connStr))
            {
                SqlMapper.SetTypeMap(typeof(PurchaseModelNonStandard_for_func),
                    new ColumnAttributeTypeMapper<PurchaseModelNonStandard_for_func>());

                var rows = context.Query<PurchaseModelNonStandard_for_func>("select * from fn_GetPurchasesByYearWithNonStandardColumns (@year)",
                    new { year = 2007 }).ToList();

                foreach (var row in rows)
                {
                    Console.WriteLine($"{row.CustomerId}, {row.Date}, {row.Description}, {row.Price}");
                }
            }
        }

        //16
        private static void CallProcedureReturnOneValue()
        {
            using (var context = new SqlConnection(connStr))
            {
                var res = context.Query<int?>(@"
                    DECLARE @rc int
                    exec @rc = PurchaseCount @year
                    select @rc", new { year = 2008 }).FirstOrDefault();

                if (res != null)
                    Console.WriteLine(res);
            }
        }
    }

    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCreate { get; set; }
    }

    public class PurchaseModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
    }

    public class CustomerAndPurchase : Customer
    {
        public PurchaseModel Purchase { get; set; }
    }

    public class CustomerAndPurchaseList : Customer
    {
        public List<PurchaseModel> Purchases { get; set; }
    }


    public class PurchaseModelNonStandard_for_proc
    {
        public int Id { get; set; }

        [Column("#cust_id")]
        public int CustomerId { get; set; }

        [Column("#date")]
        public DateTime Date { get; set; }

        [Column("#descr%")]
        public string Description { get; set; }

        [Column("price$")]
        public int Price { get; set; }
    }

    public class PurchaseModelNonStandard_for_func
    {
        public int Id { get; set; }

        [Column("Cust$ID")]
        public int CustomerId { get; set; }

        [Column("#Date")]
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
    }
}
